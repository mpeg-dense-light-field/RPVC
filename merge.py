import argparse
import numpy as np
import os
from glob import glob
import cv2
from shutil import copyfile
import json

 

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-central",
                        help="path to central camera registration json", type=str, required=True)
    parser.add_argument("-subaperture",
                        help="path to a subaperture set registration json", type=str, required=True)
    parser.add_argument("-central_depth",
                        help="path to folder containing the central views depths, computed from distant cameras", type=str, required=True)
    parser.add_argument("-subaperture_depth",
                        help="path to folder containing the central views depths, computed from subaperture", type=str, required=True)
    parser.add_argument("-out",
                        help="out file", type=str, required=True)
    
    args = parser.parse_args()
    return args

def rotz(yaw):
    return np.array( [ [ np.cos(yaw), -np.sin(yaw), 0 ], [np.sin( yaw ), np.cos(yaw), 0], [0,0,1] ])
    
def roty(pitch):
    return np.array( [ [ np.cos(pitch), 0, np.sin(pitch) ], [0,1,0], [-np.sin(pitch), 0, np.cos(pitch)] ])
    
def rotx(roll):
    return np.array( [ [1,0,0], [ 0, np.cos(roll), -np.sin(roll) ], [0, np.sin(roll), np.cos(roll)] ])

def EulerAnglesToRotationMatrix( EulerAngles ):
    yaw, pitch, roll = EulerAngles
    R = rotz(yaw).dot( roty(pitch) ).dot ( rotx(roll) )
    return R


def AllmostZero(v):
    eps = 1e-7
    return abs(v) < eps

def RotationMatrixToEulerAngles(R):
    yaw   = 0.0
    pitch = 0.0
    roll  = 0.0
    
    if AllmostZero( R[0,0] ) and AllmostZero( R[1,0] ) :
        yaw = np.arctan2( R[1,2], R[0,2] )
        if R[2,0] < 0.0:
            pitch = np.pi/2
        else:
            pitch = -np.pi/2
        roll = 0.0
    else:
        yaw = np.arctan2( R[1,0], R[0,0] )
        if AllmostZero( R[0,0] ) :
            pitch = np.arctan2( -R[2,0], R[1,0] / np.sin(yaw) )
        else:
            pitch = np.arctan2( -R[2,0], R[0,0] / np.cos(yaw) )
        
        roll = np.arctan2( R[2,1], R[2,2] )
    
    euler = np.array( [yaw, pitch, roll] )
    
    return np.rad2deg(euler)

def compute_ratio(d1,d2):
    r = d1/d2
    q11 = np.quantile(d1[d1>0],0.1)
    q19 = np.quantile(d1[d1>0],0.9)
    q21 = np.quantile(d2[d2>0],0.1)
    q29 = np.quantile(d2[d2>0],0.9)
    k = r[np.isfinite(r) * (d1 > q11) * (d1 < q19) * (d2 > q21) * (d2 < q29) * (r > 0)]
    print(np.mean(k))
    return np.mean(k)
    #print(np.quantile(k,0.1))
    #print(np.quantile(k,0.9))

def main():
    args = parse_args()
    central_depth_folder = args.central_depth
    subaperture_depth_folder = args.subaperture_depth
    
    central_names = sorted(glob(f"{central_depth_folder}/*exr"))
    subaperture_names = sorted(glob(f"{subaperture_depth_folder}/*exr"))
    
    if not (len(central_names)==len(subaperture_names)):
        print("Verify the number of files in each folder!")
    
    ratio = 0.0
    for cn,sn in zip(central_names,subaperture_names):
        d1 = cv2.imread(sn, cv2.IMREAD_ANYDEPTH)
        d2 = cv2.imread(cn, cv2.IMREAD_ANYDEPTH)
        ratio = ratio+compute_ratio(d1,d2)
    ratio = ratio/len(central_names)
    print(ratio)
    
    central_filepath = args.central
    subaperture = args.subaperture
    out_filepath = args.out

    with open(central_filepath) as centralfile:
        data = json.load(centralfile)
        cameras  = data['cameras']
    with open(subaperture) as subaperturefile:
        data_subaperture = json.load(subaperturefile)
        cameras_subaperture  = data_subaperture['cameras']
        
    out_cameras=[]
    for i,central_cam in enumerate(cameras):
        R = EulerAnglesToRotationMatrix(np.array(central_cam["Rotation"])*np.pi/180.0)
        P = ratio*np.array(central_cam["Position"])
        for j in range(25):
            idx_j = 20-5*(j//5)+(j%5)#currently the script CamParamGen exporting the cameras starts with the lowest row
            subaperture_cam = cameras_subaperture[idx_j]
            Rs = EulerAnglesToRotationMatrix(np.array(subaperture_cam["Rotation"])*np.pi/180.0)
            Ps = np.array(subaperture_cam["Position"])
            out_cameras.append(central_cam.copy())
            out_cameras[-1]["Name"] = out_cameras[-1]["Name"][:-3]+str(j+1).zfill(3)
            out_cameras[-1]["Position"] = (P+R.dot(Ps)).tolist()
            out_cameras[-1]["Rotation"] = RotationMatrixToEulerAngles(R.dot(Rs)).tolist()
            out_cameras[-1]["Depth_range"] = [0.9*subaperture_cam["Depth_range"][0],1.2*subaperture_cam["Depth_range"][1]] #add margin to estimated depth range
            

    data['cameras'] = out_cameras
    with open( out_filepath, 'w') as outfile:
        json.dump(data, outfile, indent=4)
        


if __name__ == "__main__":
    main()