#---------cfg parameter-----------
N = 5
W = 1720
H = 976
Rot = 0.003183648
B = 0.172696447
Xn = 420
Xf = 740
F = 28.12538166
P = 0.00500093
BDC = 8
BDD = 8
CS = 'YUV420'
DCS = 'YUV420'
fileName = 'RLC.json'
#---------cfg parameter-----------

C = int(N / 2 +1)
focal = F / P

f = open(fileName, 'w')
tab1 = '     '
tab2 = tab1+tab1
tab3 = tab2+tab1

f.write('{\n')
f.write(tab1 +'"cameras":\n')
f.write(tab1 +'[\n')

for row in range(1, N + 1):
    delta_row = row - C
    tz = B * delta_row
    py = H / 2
    pitch = Rot * delta_row
    for colm in range(1, N + 1):
        delta_colm = colm - C
        ty = -B * delta_colm
        px = W / 2
        yaw = Rot * delta_colm
        f.write(tab2 +'{\n')
        f.write(tab3 +'"Name": "image_'+ str(colm+N*(N-row)).zfill(3) +'",\n')
        #f.write(tab3 +'"Name": "v_x'+ str(colm) + 'y' + str(row) +'",\n')
        f.write(tab3 +'"Position": [0.0, '+ str(ty) + ', ' + str(tz)+'],\n')
        f.write(tab3 +'"Rotation": [' + str(yaw) + ', ' + str(pitch) + ', 0.0],\n')
        f.write(tab3 +'"Depth_range": [' + str(Xn) + ', ' + str(Xf) + '],\n')
        f.write(tab3 +'"Resolution": [' + str(W) + ', ' + str(H) + '],\n')
        f.write(tab3 +'"Projection": "Perspective",\n')
        f.write(tab3 +'"Focal": [' + str(focal) + ', ' + str(focal) + '],\n')
        f.write(tab3 +'"Principle_point": [' + str(px) + ', ' + str(py) + '],\n')
        f.write(tab3 +'"BitDepthColor": ' + str(BDC) + ',\n')
        f.write(tab3 +'"BitDepthDepth": ' + str(BDD) + ',\n')
        f.write(tab3 +'"ColorSpace": "' + CS + '",\n')
        f.write(tab3 +'"DepthColorSpace": "' + DCS + '"\n')
        if row == N and colm == N:
            f.write(tab2 +'}\n')
        else:
            f.write(tab2 +'},\n')
            
f.write(tab1 +']\n')
f.write('}\n')

f.close()