import json
import numpy as np
import argparse


def parse_args():
	parser = argparse.ArgumentParser()
	parser.add_argument("-c", "--cameras",
		        help="path to colmap cameras", type=str, required=True)
	parser.add_argument("-i", "--images",
		        help="path to colmap images", type=str, required=True)
	parser.add_argument("-n", "--content_name",
		        help="name of the dataset", type=str, required=False)
	parser.add_argument("-o", "--out",
		        help="path to output json file", type=str, required=True)
	args = parser.parse_args()
	return args


def AllmostZero(v):
	eps = 1e-7
	return abs(v) < eps

def RotationMatrixToEulerAngles(R):
	yaw   = 0.0
	pitch = 0.0
	roll  = 0.0
    
	if AllmostZero( R[0,0] ) and AllmostZero( R[1,0] ) :
		yaw = np.arctan2( R[1,2], R[0,2] )
		if R[2,0] < 0.0:
			pitch = np.pi/2
		else:
			pitch = -np.pi/2
		roll = 0.0
	else:
		yaw = np.arctan2( R[1,0], R[0,0] )
		if AllmostZero( R[0,0] ) :
			pitch = np.arctan2( -R[2,0], R[1,0] / np.sin(yaw) )
		else:
			pitch = np.arctan2( -R[2,0], R[0,0] / np.cos(yaw) )
        
		roll = np.arctan2( R[2,1], R[2,2] )
    
	euler = np.array( [yaw, pitch, roll] )
    
	return np.rad2deg(euler)

def QuaternionToRotationMatrix(q):
	q = q/ np.linalg.norm(q)
	R1 = np.array([[q[0],q[3],-q[2],q[1]],
		[-q[3],q[0],q[1],q[2]],
		[q[2],-q[1],q[0],q[3]],
		[-q[1],-q[2],-q[3],q[0]]])
	R2 = np.array([[q[0],q[3],-q[2],-q[1]],
		[-q[3],q[0],q[1],q[2]],
		[q[2],-q[1],q[0],-q[3]],
		[q[1],q[2],q[3],q[0]]])
	R = R1.dot(R2)
	return R
def QuaternionToRotationMatrix2(q):
	q = q/ np.linalg.norm(q)
	R = np.transpose(np.array([[2*(q[0]*q[0]+q[1]*q[1])-1, 2*(q[1]*q[2]-q[0]*q[3]),   2*(q[1]*q[3]+q[0]*q[2]),   0],
		      [2*(q[1]*q[2]+q[0]*q[3]),   2*(q[0]*q[0]+q[2]*q[2])-1, 2*(q[2]*q[3]-q[0]*q[1]),   0],
		      [2*(q[1]*q[3]-q[0]*q[2]),   2*(q[2]*q[3]+q[0]*q[1]),   2*(q[0]*q[0]+q[3]*q[3])-1, 0],
		[0,0,0,1]]))
	return R
    
def main():	
	args = parse_args()
    
	with open(args.cameras, mode='r') as file: 
		filelines = file.readlines()
	cams = filelines[3:]
	cams_param = [cam.split() for cam in cams]

	with open(args.images, mode='r') as file:
		filelines = file.readlines()
	l_ext = [filelines[i].split() for i in range(4,len(filelines),2)]
	def order(l):
	    #return l[9]
	    return (((l[9].split("/"))[-1]).split("."))[0]
	l_ext.sort(key=order)
	    
    
	metadata = {
		    'Version': '2.0', 
		    'Content_name': 'Colmap dataset',
		    'Fps': 1,
		    'Frames_number': 1,
		    'Informative': {
		        'Converted_by': 'colmap_to_JSON.py',
		        'Original_units': 'm', 
		        'New_units': 'm' },
		    'cameras': {
		        'Resolution': [1920, 1080],
		        'Depth_range': [40, 70.0],       # meters
		        'BitDepthColor': 8,
		        'BitDepthDepth': 16 }
		    }  
	if args.content_name:
		metadata['Content_name'] = args.content_name

	permute = np.array([ [0, 0, 1], [-1, 0, 0], [0, -1, 0] ] )
	#permute = np.array([ [0, -1, 0], [0, 0, -1], [1, 0, 0] ] )
	#permute = np.array([ [0, 0, -1], [-1, 0, 0], [0, -1, 0] ] )
	cameras = []      
	for l in l_ext:
		cam_param = cams_param[int(l[8])-1]  
		if cam_param[1] == 'SIMPLE_PINHOLE':
			Intrinsics       = np.fromstring( cam_param[2] + ' ' + cam_param[3] + ' ' + cam_param[4] + ' ' + cam_param[4] + ' ' + cam_param[5] + ' ' + cam_param[6], dtype=float, sep=' ' )
		else: #PINHOLE MODEL. models including distortion are not implemented in the json format
			Intrinsics       = np.fromstring( cam_param[2] + ' ' + cam_param[3] + ' ' + cam_param[4] + ' ' + cam_param[5] + ' ' + cam_param[6] + ' ' + cam_param[7], dtype=float, sep=' ' )
		Name             = (((l[9].split("/"))[-1]).split("."))[0]
		if not Name:
			break
		Resolution       = [Intrinsics[0], Intrinsics[1] ]
		Focal            = [Intrinsics[2], Intrinsics[3] ]
		Principle_point  = [Intrinsics[4], Intrinsics[5] ]


		R= (QuaternionToRotationMatrix2(np.fromstring( l[1] + ' ' + l[2] + ' ' + l[3] + ' ' + l[4], dtype=float, sep=' ' )) )
		R33 = ( (permute).dot( (R[0:3,0:3]) ).dot( np.transpose(permute) ) ) #todo check transpose
		EulerAngles      = RotationMatrixToEulerAngles(R33);
		Position = np.fromstring( l[5] + ' ' + l[6] + ' ' + l[7]+ " 1", dtype=float, sep=' ' )
		#Position = -np.transpose(R).dot(Position)
		Position = -R.dot(Position)
		Position = permute.dot(np.array([Position[0],Position[1],Position[2]]))
		#Position = np.array([Position[2],-Position[0],-Position[1]])

		cam = {   'Name': Name,
		    'Position': Position.tolist(), 
		    'Rotation': EulerAngles.tolist(),
		    'Depthmap': 1,
		    'Background': 0,
		    'Depth_range': metadata['cameras']['Depth_range'],
		    'Resolution': Resolution,
		    'Projection': 'Perspective',
		    'Focal': Focal,
		    'Principle_point': Principle_point,
		    'BitDepthColor': metadata['cameras']['BitDepthColor'],
		    'BitDepthDepth': metadata['cameras']['BitDepthDepth'],
		    'ColorSpace': 'YUV420',
		    'DepthColorSpace': 'YUV420' }

		cameras.append(cam)
    
	metadata['cameras'] = cameras      

	with open( args.out, 'w') as outfile:
	    json.dump(metadata, outfile, indent=4)

if __name__ == "__main__":
	main()
