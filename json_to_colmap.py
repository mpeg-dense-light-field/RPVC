import json
import numpy as np
import argparse


def parse_args():
	parser = argparse.ArgumentParser()
	parser.add_argument("-c", "--cameras",
		        help="path to json cameras", type=str, required=True)
	parser.add_argument("-oc", "--cam_out",
		        help="path to output cam file", type=str, required=True)
	parser.add_argument("-oi", "--img_out",
		        help="path to output img file", type=str, required=True)
	args = parser.parse_args()
	return args


def AllmostZero(v):
	eps = 1e-7
	return abs(v) < eps
	
	
def rotz(yaw):
    return np.array( [ [ np.cos(yaw), -np.sin(yaw), 0 ], [np.sin( yaw ), np.cos(yaw), 0], [0,0,1] ])
    
def roty(pitch):
    return np.array( [ [ np.cos(pitch), 0, np.sin(pitch) ], [0,1,0], [-np.sin(pitch), 0, np.cos(pitch)] ])
    
def rotx(roll):
    return np.array( [ [1,0,0], [ 0, np.cos(roll), -np.sin(roll) ], [0, np.sin(roll), np.cos(roll)] ])

def EulerAnglesToRotationMatrix( EulerAngles ):
    yaw, pitch, roll = EulerAngles
    R = rotz(yaw).dot( roty(pitch) ).dot ( rotx(roll) )
    return R

def RotationMatrixToEulerAngles(R):
	yaw   = 0.0
	pitch = 0.0
	roll  = 0.0
    
	if AllmostZero( R[0,0] ) and AllmostZero( R[1,0] ) :
		yaw = np.arctan2( R[1,2], R[0,2] )
		if R[2,0] < 0.0:
			pitch = np.pi/2
		else:
			pitch = -np.pi/2
		roll = 0.0
	else:
		yaw = np.arctan2( R[1,0], R[0,0] )
		if AllmostZero( R[0,0] ) :
			pitch = np.arctan2( -R[2,0], R[1,0] / np.sin(yaw) )
		else:
			pitch = np.arctan2( -R[2,0], R[0,0] / np.cos(yaw) )
        
		roll = np.arctan2( R[2,1], R[2,2] )
    
	euler = np.array( [yaw, pitch, roll] )
    
	return np.rad2deg(euler)

def QuaternionToRotationMatrix(q):
	q = q/ np.linalg.norm(q)
	R1 = np.array([[q[0],q[3],-q[2],q[1]],
		[-q[3],q[0],q[1],q[2]],
		[q[2],-q[1],q[0],q[3]],
		[-q[1],-q[2],-q[3],q[0]]])
	R2 = np.array([[q[0],q[3],-q[2],-q[1]],
		[-q[3],q[0],q[1],q[2]],
		[q[2],-q[1],q[0],-q[3]],
		[q[1],q[2],q[3],q[0]]])
	R = R1.dot(R2)
	return R
def QuaternionToRotationMatrix2(q):
	q = q/ np.linalg.norm(q)
	R = np.transpose(np.array([[2*(q[0]*q[0]+q[1]*q[1])-1, 2*(q[1]*q[2]-q[0]*q[3]),   2*(q[1]*q[3]+q[0]*q[2]),   0],
		      [2*(q[1]*q[2]+q[0]*q[3]),   2*(q[0]*q[0]+q[2]*q[2])-1, 2*(q[2]*q[3]-q[0]*q[1]),   0],
		      [2*(q[1]*q[3]-q[0]*q[2]),   2*(q[2]*q[3]+q[0]*q[1]),   2*(q[0]*q[0]+q[3]*q[3])-1, 0],
		[0,0,0,1]]))
	return R
	
	
def RotMat2Quaternions(R):
    tr = R[0][0] + R[1][1] + R[2][2]
    if tr > 0:
        w = np.sqrt(tr+1.0)/2.0
        q = [
            w,
            (-R[2][1]+R[1][2])/(4.0*w),
            (-R[0][2]+R[2][0])/(4.0*w),
            (-R[1][0]+R[0][1])/(4.0*w)
        ]
    elif (R[0][0] > R[1][1]) and (R[0][0] > R[2][2]): 
        S = np.sqrt(1.0 + R[0][0] - R[1][1] - R[2][2]) * 2; 
        q = [ (R[1][2] - R[2][1]) / S,
              0.25 * S,
              (R[1][0] + R[0][1]) / S,
              (R[2][0] + R[0][2]) / S
        ]   
    elif (R[1][1] > R[2][2]): 
        S = np.sqrt(1.0 + R[1][1] - R[0][0] - R[2][2]) * 2; 
        q = [ (R[2][0] - R[0][2]) / S,
              (R[1][0] + R[0][1]) / S,
              0.25 * S,
              (R[2][1] + R[1][2]) / S
        ]   
    else: 
        S = np.sqrt(1.0 + R[2][2] - R[0][0] - R[1][1]) * 2; 
        q = [ (R[0][1] - R[1][0]) / S,
              (R[2][0] + R[0][2]) / S,
              (R[2][1] + R[1][2]) / S,
              0.25 * S
        ]
    return q
    
    
def main():	
	args = parse_args()
	with open(args.cameras) as infile:
		data = json.load(infile)
		cameras  = data['cameras']
	
	l_int = []
	l_ext = []
	i_image = 0
	i_camera = 0
	
	permute = np.array([ [0, 0, 1], [-1, 0, 0], [0, -1, 0] ] )
	for cam in cameras:
		i_image+=1
		intrinsic = "PINHOLE "+str(cam['Resolution'][0]) + " "+str(cam['Resolution'][1]) + " "+str(cam['Focal'][0]) + " "+str(cam['Focal'][1]) + " "+str(cam['Principle_point'][0]) + " "+str(cam['Principle_point'][1])
		try:
		    idx = l_int.index(intrinsic)+1
		except Exception:
		    idx = -1
		if idx < 0:
		    i_camera+=1
		    idx = i_camera
		    l_int.append(intrinsic)      
		EulerAngles = np.pi/180.0*np.array(cam['Rotation'])
		RMat = EulerAnglesToRotationMatrix(EulerAngles)
		RMat2 = ( np.transpose(permute).dot( (RMat )).dot( (permute) ) )
		rotation = RotMat2Quaternions(RMat2)
		position = cam['Position']
		position = (np.transpose(permute)).dot(np.array([position[0],position[1],position[2]]))
		position = -np.transpose(RMat2).dot(position)
		name = cam['Name']+".png"
		l_ext.append(str(i_image)+ " " +str(rotation[0])+ " " +str(rotation[1])+ " " +str(rotation[2])+ " " +str(rotation[3])+ " " +str(position[0])+ " " +str(position[1])+ " " +str(position[2])+ " "+str(idx)+" " +name)
		
    
	with open(args.cam_out, mode='w') as file: 
		file.write("# Camera list with one line of data per camera:\n")
		file.write("#   CAMERA_ID, MODEL, WIDTH, HEIGHT, PARAMS[]\n")
		file.write("# Number of cameras: 1\n")
		for i,l in enumerate(l_int):
		    file.write(str(i+1)+" "+l+"\n")
	with open(args.img_out, mode='w') as file: 
		file.write("# Image list with two lines of data per image:\n")
		file.write("#   IMAGE_ID, QW, QX, QY, QZ, TX, TY, TZ, CAMERA_ID, NAME\n")
		file.write("#   POINTS2D[] as (X, Y, POINT3D_ID)\n")
		file.write("# Number of images:"+str(len(l_ext))+", mean observations per image: 0\n")
		for l in l_ext:
			file.write(l+"\n\n")


if __name__ == "__main__":
	main()
