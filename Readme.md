# License

## Term of Use
ANY kind of publication or report using this tool MUST refer to the following references 

 - Sarah Fachada, Armand Losfeld, Takanori Senoh, Gauthier Lafruit, and Mehrdad Teratani, “A Calibration Method for Subaperture Views of Plenoptic 2.0 Camera Arrays,” in 2021 IEEE 23nd International Workshop on Multimedia Signal Processing, Tampere, Finland, Oct. 2021.

@inproceedings{fachada_calibration_2021,
	address = {Tampere, Finland},
	title = {A {Calibration} {Method} for {Subaperture} {Views} of {Plenoptic} 2.0 {Camera} {Arrays}},
	booktitle = {2021 {IEEE} 23nd {International} {Workshop} on {Multimedia} {Signal} {Processing}},
	author = {Fachada, Sarah and Losfeld, Armand and Senoh, Takanori and Lafruit, Gauthier and Teratani, Mehrdad},
	month = oct,
	year = {2021}
	}

 - Sarah Fachada, Armand Losfeld, Tankanori Senoh, Daniele Bonatto, Gauthier Lafruit, Mehrdad Teratani, "[LVC] [DLF] A new calibration tool for multi-plenoptic 2.0 cameras ", ISO/IEC JTC 1/SC 29/WG 4 m57837, Online,  October 2021.

@article{fachada_lvc_2021,
	address = {Online},
	edition = {MPEG2021/M57837},
	title = {[{LVC}] [{DLF}] {A} new calibration tool for multi-plenoptic 2.0 cameras [m57837]},
	journal = {ISO/IEC JTC1/SC29/WG11},
	author = {Fachada, Sarah and Losfeld, Armand and Senoh, Takanori and Bonatto, Daniele and Lafruit, Gauthier and Teratani, Mehrdad},
	month = oct,
	year = {2021},
	doi = {10.5281/zenodo.4488243},
}

## Copyright
ONLY Available for Academic and Standardization Usage

## Production
Université Libre de Bruxelles (Belgium), Tokyo Denki University (Japan)

# Calibration tool for multi-plenoptic cameras

This is a step by step howto to register the subaperture views of multiple plenoptic cameras starting with the lenslet images.
Each step is a building block that can be replaced by similar tool (for example the use of Colmap for registration)
We provide an example for RabbitStamp sequence for the merging step.

## Create the subaperture views with RLC [1-2]
```
RLC parameters.cfg
```

## Register the central views with COLMAP [3]
1. Create a folder with all the central views of each subaperture set (*image_013.png)
2. Feature extration
```
colmap feature_extractor \
    --database_path $PROJECT_PATH/central_views/database.db \
    --image_path $PROJECT_PATH/central_views/images \
	--ImageReader.camera_model SIMPLE_PINHOLE \
	--ImageReader.single_camera 1
```
3. Feature matching
```
colmap exhaustive_matcher \
    --database_path $PROJECT_PATH/central_views/database.db 
```
4. Registration
```
colmap mapper \
    --database_path $PROJECT_PATH/central_views/database.db \
    --image_path $PROJECT_PATH/central_views/images \
    --output_path $PROJECT_PATH/central_views/sparse
```
4. Convertion to json file
```
colmap model_converter \
    --input_path $PROJECT_PATH/central_views/sparse \
    --output_path $PROJECT_PATH/central_views/sparse \
    --output_type TXT
```
```
python colmap_to_json.py -c $DATASET_PATH/central_views/sparse/cameras.txt -i  $DATASET_PATH/central_views/sparse/images.txt [-n CONTENT_NAME] -o CENTRAL_CAMERA.JSON
```

## Compute the subaperture calibration with excel/python [4-5]
For the focal length in pixels, you can use the one found by Colmap
```
python CamParamGen.py
```

## Compute the Colmap's models for each subaperture view set
1. Create a folder with a set of subaperture images ($PROJECT_PATH/subaperture_XX/images)
2. Convert json to Colmap file format
```python json_to_colmap.py [-h] -c CAMERAS -oc CAM_OUT -oi IMG_OUT```

3. Feature extration
```
colmap feature_extractor \
    --database_path $PROJECT_PATH/subaperture_XX/database.db \
    --image_path $PROJECT_PATH/subaperture_XX/images 
```
4. Feature matching
```
colmap exhaustive_matcher \
    --database_path $PROJECT_PATH/subaperture_XX/database.db 
```
5. Triangulation (set low angle values, due to small baseline)
```
colmap point_triangulator \
    --database_path $PROJECT_PATH/subaperture_XX/database.db \
    --image_path $PROJECT_PATH/subaperture_XX/images \
    --input_path $PROJECT_PATH/subaperture_XX/sparse \
    --output_path $PROJECT_PATH/subaperture_XX/triangulated \
    --Mapper.tri_min_angle 0.02 \
    --Mapper.local_ba_min_tri_angle 0.03 \
    --Mapper.filter_min_tri_angle 0.02
```
6. Repeat steps 1-5 for all the subaperture view you want to use in the registration

## Compute Colmap's depth maps
Compute the depth maps that will be used for the scaling:
- Depth maps for the central views
- Depth maps for at least one subaperture set
1.
```
colmap image_undistorter \
    --image_path $DATASET_PATH/images \
    --input_path $DATASET_PATH/sparse \
    --output_path $DATASET_PATH/dense \
    --output_type COLMAP \
    --max_image_size 2000
```
```
$ colmap patch_match_stereo \
    --workspace_path $DATASET_PATH/dense \
    --workspace_format COLMAP \
    --PatchMatchStereo.geom_consistency true
```
2. Convert to exr format
```
python dense_to_exr.py [-h] -d DEPTH_MAP [-t DEPTHTYPE (geometric or photometric, default geometric)]
```

## Merge the two registrations [5]
1. Prepare two exr depth maps folders (see example): 
- One with depth maps computed with the Colmap model including only central views (CENTRAL_DEPTH_FOLDER)
- One with the corresponding depth maps computed from the subaperture images (SUBAPERTURE_DEPTH_FOLDER)
2. Obtain a json file with all cameras:
```
python merge.py -central CENTRAL_CAMERA.JSON -subaperture SUBAPERTURE_CAMERA.JSON -central_depth CENTRAL_DEPTH_FOLDER -subaperture_depth SUBAPERTURE_DEPTH_FOLDER 
``` 

# References
[1] M. Teratani, W. Ouyang, and T. Fujii, “[MPEG-I Visual] Conversion of Lenslet Data Capture by Single-Focused Plenoptic Camera to Multiview video Using RLC0.3 [M50635],” ISO/IEC JTC1/SC29/WG11, Geneva, Switzerland, Oct. 2019.
[2] S. Fachada, D. Bonatto, G. Lafruit, and M. Teratani, “[DLF] Update for Reference Lenslet content Convertor (RLC) [M56418],” ISO/IEC JTC1/SC29/WG11, Online, Apr. 2021.
[3] J. L. Schonberger and J.-M. Frahm, “Structure-from-Motion Revisited,” in 2016 IEEE Conference on Computer Vision and Pattern Recognition (CVPR), Las Vegas, NV, USA, Jun. 2016, pp. 4104–4113. 
[4] T. Senoh and N. Tetsutani, “[DLF] Camera Parameter Generation from Plenoptic2 Image  [M56349],” ISO/IEC JTC1/SC29/WG11, Online, p. 15, Apr. 2021.
[5] S. Fachada, A. Losfeld, T. Senoh, G. Lafruit, and M. Teratani, “A Calibration Method for Subaperture Views of Plenoptic 2.0 Camera Arrays,” in 2021 IEEE 23nd International Workshop on Multimedia Signal Processing, Tampere, Finland, Oct. 2021.
